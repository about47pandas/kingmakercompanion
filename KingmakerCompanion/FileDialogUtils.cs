﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Xml;
using Microsoft.Win32;

namespace KingmakerCompanion
{
    public static class FileDialogUtils
    {
        public static OpenFileDialog OpenFileDialogResult(string fileTypes, params string[] fileExtensions)
        {

            OpenFileDialog ofDialog = new OpenFileDialog();
            ofDialog.Filter = FormatExtensions(fileTypes, fileExtensions);
            ofDialog.CheckPathExists = true;
            ofDialog.CheckFileExists = true;
            ofDialog.Multiselect = false;

            return ofDialog;
        }

        public static void GenericSerialize(object data, string fileType, string extension)
        {

            SaveFileDialog sfDialog = new SaveFileDialog();
            sfDialog.Filter = fileType + ": (*" + extension + ")|*" + extension;
            sfDialog.CheckPathExists = true;
            sfDialog.OverwritePrompt = true;
            sfDialog.ValidateNames = true;

            bool? result = sfDialog.ShowDialog();

            if (result == null || result.Value == false)
            {
                return;
            }

            Serialize(sfDialog.FileName, data);
        }

        public static void Serialize<T>(string filename, T data)
        {
            XmlWriterSettings settings = getXmlWriterSettings();

            using (XmlWriter writer = XmlWriter.Create(filename, settings))
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(data.GetType());
                x.Serialize(writer, data);
            }
        }

        public static async Task SerializeAsync<T>(string filename, T data)
        {
            await Task.Run(() =>
            {
                lock (data)
                {
                    Serialize(filename, data);
                }
            });
        }

        public static T Deserialize<T>(string filename)
        {
            T data;

            XmlReaderSettings xmlReaderSettings = new XmlReaderSettings
            {
                CloseInput = true
            };

            using (FileStream stream = new FileStream(filename, FileMode.Open))
            using (XmlReader reader = XmlReader.Create(stream, xmlReaderSettings))
            {
                System.Xml.Serialization.XmlSerializer x = new System.Xml.Serialization.XmlSerializer(typeof(T));
                data = (T)x.Deserialize(reader);

            }

            return data;
        }

        public static Task<T> DeserializeAsync<T>(string filename)
        {
            return Task.Run(() =>
            {
                return Deserialize<T>(filename);
            });
        }

        public static string FormatExtensions(string fileTypes, string[] fileExtensions)
        {
            string filter = fileTypes;
            string baseTypes = "(";
            string searchTypes = string.Empty;

            for (int i = 0; i < fileExtensions.Length; i++)
            {
                //Check to see if the first character is a dot (even though it should be anyway)
                if (fileExtensions[i][0] != '.')
                {
                    List<char> extenString = fileExtensions[i].ToList();
                    List<char> newString = new List<char>();
                    newString.Add('.');
                    foreach (char c in extenString)
                    {
                        newString.Add(c);
                    }
                    fileExtensions[i] = new string(newString.ToArray());
                }

                if (i == fileExtensions.Length - 1)
                {
                    searchTypes += "*" + fileExtensions[i];
                    baseTypes += "*" + fileExtensions[i] + ")|";
                    break;
                }
                searchTypes += "*" + fileExtensions[i] + ";";
                baseTypes += "*" + fileExtensions[i] + ", ";
            }
            filter += ": " + baseTypes + searchTypes;
            return filter;
        }

        private static XmlWriterSettings getXmlWriterSettings()
        {
            return new XmlWriterSettings
            {
                Indent = true,
                CloseOutput = true
            };
        }
    }
}
