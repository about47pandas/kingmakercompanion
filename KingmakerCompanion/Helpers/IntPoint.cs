﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingmakerCompanion.Helpers
{
    public class IntPoint
    {
        public int X { get; set; }
        public int Y { get; set; }

        public IntPoint(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator == (IntPoint a, IntPoint b)
        {
            return a.X == b.X && a.Y == b.Y;
        }

        public static bool operator != (IntPoint a, IntPoint b)
        {
            return !(a == b);
        }

        public override bool Equals(object o)
        {
            if (!(o.GetType() == typeof(IntPoint)))
            {
                return false;
            }
            IntPoint point = o as IntPoint;
            return this == point;
        }

        public override int GetHashCode()
        {
            var hashCode = 1861411795;
            hashCode = hashCode * -1521134295 + X.GetHashCode();
            hashCode = hashCode * -1521134295 + Y.GetHashCode();
            return hashCode;
        }
    }
}
