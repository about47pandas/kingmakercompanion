﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DonJonGM.Debug;

namespace KingmakerCompanion
{
    public class TileImprovement
    {
        public string Name { get; set; }
        public int Cost { get; set; } = 0;

        public TileImprovement(string name, int cost)
        {
            Name = name;
            Cost = cost;
        }

        public TileImprovement(string name, Func<int> costFunc)
        {
            Name = name;
            Cost = costFunc();
        }

        public TileImprovement Aqueduct => new TileImprovement("Aqueduct", 0);

        public TileImprovement Canal => new TileImprovement("Canal", 0);

        public TileImprovement Farm(string tileType) {
            return new TileImprovement("Farm", () => {
                switch (tileType)
                {
                    case Constants.FarmTileTypes.Plains:
                        return 2;
                    case Constants.FarmTileTypes.Hills:
                        return 4;
                    case Constants.FarmTileTypes.Desert:
                        return 8;
                    default:
                        Tracer.TraceError("Tile Type for Farm was not valid, must be ");
                        throw new ArgumentOutOfRangeException(tileType);
                }
            });
        }

        #region Default Tile Improvement Types

        public TileImprovement Fishery => new TileImprovement("Fishery", 4);

        public TileImprovement Fort => new TileImprovement("Fort", 24);

        public TileImprovement Highway => new TileImprovement("Highway", 0);

        public TileImprovement Mine => new TileImprovement("Mine", 6);

        public TileImprovement Quarry => new TileImprovement("Quarry", 6);

        public TileImprovement Road => new TileImprovement("Road", 0);

        public TileImprovement Sawmill => new TileImprovement("Sawmill", 3);

        public TileImprovement Watchtower => new TileImprovement("Watchtower", 12);
        
        #endregion  
    }
}
