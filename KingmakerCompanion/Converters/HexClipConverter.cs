﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Data;
using System.Windows.Media;

namespace KingmakerCompanion.Converters
{
    public class HexClipConverter : IMultiValueConverter
    {
        public object Convert(object[] values, Type targetType, object parameter, CultureInfo culture)
        {
            double w = (double)values[0];
            double h = (double)values[1];
            if (w <= 0 || h <= 0)
            {
                return null;
            }

            PathFigure figure = new PathFigure
                {
                    StartPoint = new Point(w * 0.5, 0),
                    Segments =
                      {
                          new LineSegment {Point = new Point(w, h*0.25)},
                          new LineSegment {Point = new Point(w, h*0.75)},
                          new LineSegment {Point = new Point(w*0.5, h)},
                          new LineSegment {Point = new Point(0, h*0.75)},
                          new LineSegment {Point = new Point(0, h*0.25)},
                      }
                };
            return new PathGeometry { Figures = { figure } };
        }

        public object[] ConvertBack(object value, Type[] targetTypes, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
