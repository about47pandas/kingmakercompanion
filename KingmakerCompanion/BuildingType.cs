﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingmakerCompanion
{
    public class BuildingType
    {
        private static Dictionary<string, BuildingType> _buildingTypes;

        public string Name { get; set; }
        public int Time { get; set; }
        public int Cost { get; set; }
        public int Lots { get; set; }
        public string Description { get; set; }

        public BuildingType() { }

        public BuildingType(string name, int time, int cost, int lots, string description)
        {
            Name = name;
            Time = time;
            Cost = cost;
            Lots = lots;
            Description = description;
        }

        #region Default Building Types

        public static BuildingType Academy { get; } = new BuildingType(Constants.BuildingTypeNames.Academy, 4, 6, 2, "An institution of higher learning.");

        public static BuildingType Alchemist { get; } = new BuildingType(Constants.BuildingTypeNames.Alchemist, 3, 6, 1, "The laboratory and home of a crafter of poisons, potions, or alchemical items.");

        public static BuildingType Arena { get; } = new BuildingType(Constants.BuildingTypeNames.Arena, 10, 4, 4, "A large public structure for competitions and team sports.");

        public static BuildingType Bank { get; } = new BuildingType(Constants.BuildingTypeNames.Bank, 7, 4, 1, "A secure building for storing valuables and granting loans.");

        public static BuildingType BardicCollege { get; } = new BuildingType(Constants.BuildingTypeNames.BardicCollege, 8, 5, 2, "A center for artistic learning. Education in a Bardic College also includes research into a wide-range of historical topics.");

        public static BuildingType Barracks { get; } = new BuildingType(Constants.BuildingTypeNames.Barracks, 1, 6, 1, "A building to house conscripts, guards, militia, soldiers, or similar military forces.");

        public static BuildingType Baths { get; } = new BuildingType(Constants.BuildingTypeNames.Baths, 1, 4, 1, "A public building for bathing, often with hot running water and mineral soaks, sometimes heated by furnaces and other times by natural hot springs.");

        public static BuildingType BlackMarket { get; } = new BuildingType(Constants.BuildingTypeNames.BlackMarket, 5, 10, 1, "A number of shops with secret and usually illegal wares.");

        public static BuildingType Brewery { get; } = new BuildingType(Constants.BuildingTypeNames.Brewery, 1, 6, 1, "A building for beer brewing, winemaking, or some similar use.");

        public static BuildingType Bridge { get; } = new BuildingType(Constants.BuildingTypeNames.Bridge, 1, 6, 0, "Allows travel across a river or Waterway, easing transportation.");

        public static BuildingType Brothel { get; } = new BuildingType(Constants.BuildingTypeNames.Brothel, 1, 4, 1, "An establishment for dancing, drinking, carousing, and holding celebrations.");

        public static BuildingType Bureau { get; } = new BuildingType(Constants.BuildingTypeNames.Bureau, 2, 5, 2, "A large warren of offices for clerks and record-keepers working for a guild or government.");

        public static BuildingType CastersTower { get; } = new BuildingType(Constants.BuildingTypeNames.CastersTower, 6, 6, 1, "The home and laboratory for a spellcaster.");

        public static BuildingType Castle { get; } = new BuildingType(Constants.BuildingTypeNames.Castle, 12, 5, 4, "The home of the settlement’s leader or the heart of its defenses.");

        public static BuildingType Cathedral { get; } = new BuildingType(Constants.BuildingTypeNames.Cathedral, 12, 5, 4, "The focal point of the settlement’s spiritual leadership.");

        public static BuildingType Cistern { get; } = new BuildingType(Constants.BuildingTypeNames.Cistern, 1, 6, 0, "Contains a safe supply of fresh water for the settlement.");

        public static BuildingType CityWall { get; } = new BuildingType(Constants.BuildingTypeNames.CityWall, 1, 2, 0, "A fortification of one side of a district with a sturdy wall. The GM may allow for cliffs and other natural features to function as a City Wall for one or more sides of a district. You may construct gates through your own city wall at no cost.");

        public static BuildingType Courthouse { get; } = new BuildingType(Constants.BuildingTypeNames.Courthouse, 4, 4, 1, "A hall of justice, for hearing cases and resolving disputes by the rule of law. ");

        public static BuildingType Dump { get; } = new BuildingType(Constants.BuildingTypeNames.Dump, 1, 4, 1, "A centralized place to dispose of refuse.");

        public static BuildingType EverflowingSpring { get; } = new BuildingType(Constants.BuildingTypeNames.EverflowingSpring, 1, 5, 0, "A fountain built around several decanters of endless water that provides an inexhaustible supply of fresh water.");

        public static BuildingType ExoticArtisan { get; } = new BuildingType(Constants.BuildingTypeNames.ExoticArtisan, 2, 5, 1, "The shop and home of a jeweler, tinker, glassblower, or the like.");

        public static BuildingType ForeignQuarter { get; } = new BuildingType(Constants.BuildingTypeNames.ForeignQuarter, 6, 5, 4, "An area with many foreigners, as well as shops and services catering to them.");

        public static BuildingType Foundry { get; } = new BuildingType(Constants.BuildingTypeNames.Foundry, 4, 4, 2, "Processes ore and refines it into finished metal.");

        public static BuildingType Garrison { get; } = new BuildingType(Constants.BuildingTypeNames.Garrison, 6, 5, 2, "A large building to house armies, train guards, and recruit militia.");

        public static BuildingType Granary { get; } = new BuildingType(Constants.BuildingTypeNames.Granary, 2, 5, 1, "A place to store grain and food.");

        public static BuildingType Graveyard { get; } = new BuildingType(Constants.BuildingTypeNames.Graveyard, 1, 4, 1, "A plot of land to honor and bury the dead.");

        public static BuildingType Guildhall { get; } = new BuildingType(Constants.BuildingTypeNames.Guildhall, 6, 6, 2, "The headquarters for a guild or similar organization.");

        public static BuildingType Herbalist { get; } = new BuildingType(Constants.BuildingTypeNames.Herbalist, 2, 5, 1, "The workshop and home of a gardener, healer, or poisoner.");

        public static BuildingType Hospital { get; } = new BuildingType(Constants.BuildingTypeNames.Hospital, 6, 5, 2, "A building devoted to healing the sick.");

        public static BuildingType House { get; } = new BuildingType(Constants.BuildingTypeNames.House, 1, 4, 1, "A number of mid-sized houses for citizens.");

        public static BuildingType Inn { get; } = new BuildingType(Constants.BuildingTypeNames.Inn, 2, 5, 1, "A place for visitors to rest.");

        public static BuildingType Jail { get; } = new BuildingType(Constants.BuildingTypeNames.Jail, 3, 4, 1, "A fortified structure for confining criminals or dangerous monsters.");

        public static BuildingType Library { get; } = new BuildingType(Constants.BuildingTypeNames.Library, 1, 6, 1, "A large building containing an archive of books.");

        public static BuildingType Lighthouse { get; } = new BuildingType(Constants.BuildingTypeNames.Lighthouse, 4, 6, 1, "A high tower with a signal light to guide ships at sea and keep watch on waves and weather.");

        public static BuildingType Lumberyard { get; } = new BuildingType(Constants.BuildingTypeNames.Lumberyard, 2, 6, 2, "A mill and carpentry works for producing precut logs, boards, and wood products for construction.");

        public static BuildingType LuxuryStore { get; } = new BuildingType(Constants.BuildingTypeNames.LuxuryStore, 4, 7, 1, "A shop that specializes in expensive comforts for the wealthy.");

        public static BuildingType MagicShop { get; } = new BuildingType(Constants.BuildingTypeNames.MagicShop, 8, 8, 1, "A shop that specializes in magic items and spells.");

        public static BuildingType MagicalAcademy { get; } = new BuildingType(Constants.BuildingTypeNames.MagicalAcademy, 10, 6, 2, "An institution for training students in spellcasting, magic item crafting, and various arcane arts.");

        public static BuildingType MagicStreetlamps { get; } = new BuildingType(Constants.BuildingTypeNames.MagicStreetlamps, 1, 5, 0, "Continual flame (spell) lamps that illuminate the lot.");

        public static BuildingType Mansion { get; } = new BuildingType(Constants.BuildingTypeNames.Mansion, 2, 5, 1, "A single huge manor housing a rich family and its servants.");

        public static BuildingType Market { get; } = new BuildingType(Constants.BuildingTypeNames.Market, 6, 8, 2, "An open area for traveling merchants and bargain hunters.");

        public static BuildingType Menagerie { get; } = new BuildingType(Constants.BuildingTypeNames.Menagerie, 4, 4, 4, "A large park stocked with exotic creatures for public viewing.");

        public static BuildingType MilitaryAcademy { get; } = new BuildingType(Constants.BuildingTypeNames.MilitaryAcademy, 6, 6, 2, "An institution dedicated to the study of war and the training of elite soldiers and officers.");

        public static BuildingType Mill { get; } = new BuildingType(Constants.BuildingTypeNames.Mill, 2, 4, 1, "A building used to cut lumber or grind grain.");

        public static BuildingType Mint { get; } = new BuildingType(Constants.BuildingTypeNames.Mint, 5, 6, 1, "A secure building where the kingdom’s coinage is minted and standard weights and measures are kept.");

        public static BuildingType Moat { get; } = new BuildingType(Constants.BuildingTypeNames.Moat, 1, 2, 0, "A fortification of one side of a district with an open or water-filled ditch, often backed by a low dike or embankment. The GM may allow a river or similar natural feature to function as a moat for one or more sides of a district.");

        public static BuildingType Monastery { get; } = new BuildingType(Constants.BuildingTypeNames.Monastery, 4, 4, 2, "A cloister for meditation, study, and the pursuit of various other scholarly paths.");

        public static BuildingType Monument { get; } = new BuildingType(Constants.BuildingTypeNames.Monument, 1, 4, 1, "A local memorial such as a bell tower, a statue of a settlement founder, a large tomb, or a public display of art.");

        public static BuildingType Museum { get; } = new BuildingType(Constants.BuildingTypeNames.Museum, 5, 6, 2, "A place to display art and artifacts both modern and historical. The GM may allow the kingdom leaders to display a valuable item (such as a magic item or bejeweled statue) in the museum, increasing Fame during this display by 1 for every 10,000 gp of the item’s price (maximum +5 Fame), and by an additional 1 if the item is significant to the kingdom’s history.");

        public static BuildingType NobleVilla { get; } = new BuildingType(Constants.BuildingTypeNames.NobleVilla, 4, 6, 2, "A sprawling manor with luxurious grounds that houses a noble’s family and staff.");

        public static BuildingType Observatory { get; } = new BuildingType(Constants.BuildingTypeNames.Observatory, 4, 3, 1, "A dome or tower with optical devices for viewing the heavens.");

        public static BuildingType Palace { get; } = new BuildingType(Constants.BuildingTypeNames.Palace, 18, 6, 4, "A grand edifice and walled grounds demonstrating one’s wealth, power, and authority to the world.");

        public static BuildingType Park { get; } = new BuildingType(Constants.BuildingTypeNames.Park, 1, 4, 1, "A plot of land set aside for its serene beauty.");

        public static BuildingType PavedStreets { get; } = new BuildingType(Constants.BuildingTypeNames.PavedStreets, 6, 4, 0, "Brick or stone pavement that speeds transportation.");

        public static BuildingType Pier { get; } = new BuildingType(Constants.BuildingTypeNames.Pier, 4, 4, 1, "Warehouses and workshops for docking ships and handling cargo and passengers.");

        public static BuildingType SewerSystem { get; } = new BuildingType(Constants.BuildingTypeNames.SewerSystem, 6, 4, 0, "An underground sanitation system that keeps the settlement clean, though it may become home to criminals and monsters.");

        public static BuildingType Shop { get; } = new BuildingType(Constants.BuildingTypeNames.Shop, 1, 8, 1, "A general store.");

        public static BuildingType Shrine { get; } = new BuildingType(Constants.BuildingTypeNames.Shrine, 1, 8, 1, "A shrine, idol, sacred grove, or similar holy site designed for worship by pious individuals.");

        public static BuildingType Smithy { get; } = new BuildingType(Constants.BuildingTypeNames.Smithy, 1, 6, 1, "The workshop of an armorsmith, blacksmith, weaponsmith, or other craftsman who works with metal.");

        public static BuildingType Stable { get; } = new BuildingType(Constants.BuildingTypeNames.Stable, 2, 5, 1, "A structure for housing or selling horses and other mounts.");

        public static BuildingType Stockyard { get; } = new BuildingType(Constants.BuildingTypeNames.Stockyard, 4, 5, 4, "Barns and pens that store herd animals and prepare them for nearby slaughterhouses.");

        public static BuildingType Tavern { get; } = new BuildingType(Constants.BuildingTypeNames.Tavern, 2, 6, 1, "An eating or drinking establishment.");

        public static BuildingType Temple { get; } = new BuildingType(Constants.BuildingTypeNames.Temple, 5, 6, 2, "A large place of worship dedicated to a deity.");

        public static BuildingType Tenement { get; } = new BuildingType(Constants.BuildingTypeNames.Tenement, 1, 1, 1, "A staggering number of low-rent housing units.");

        public static BuildingType Theater { get; } = new BuildingType(Constants.BuildingTypeNames.Theater, 6, 4, 2, "A venue for entertainments such as plays, operas, and concerts.");

        public static BuildingType TownHall { get; } = new BuildingType(Constants.BuildingTypeNames.TownHall, 4, 5, 2, "A public venue for town meetings, repository for town records, and offices for minor bureaucrats.");

        public static BuildingType TradeShop { get; } = new BuildingType(Constants.BuildingTypeNames.TradeShop, 2, 5, 1, "A shop front for a tradesperson, such as a baker, butcher, candle maker, cobbler, rope maker, or wainwright.");

        public static BuildingType Tunnels { get; } = new BuildingType(Constants.BuildingTypeNames.Tunnels, 2, 8, 0, "An extensive set of subterranean chambers, vaults, and tunnels, usually used for storage or burial, and sometimes for illicit activities. When used for burials, Tunnels are also called Catacombs. ");

        public static BuildingType University { get; } = new BuildingType(Constants.BuildingTypeNames.University, 8, 6, 4, "An institution of higher learning, focusing mainly on mundane subjects but dabbling in magical theory.");

        public static BuildingType Warehouse { get; } = new BuildingType(Constants.BuildingTypeNames.Warehouse, 1, 8, 2, "A cavernous structure or cluster of buildings for storage and transfer of trade goods.");

        public static BuildingType Watchtower { get; } = new BuildingType(Constants.BuildingTypeNames.Watchtower, 2, 6, 1, "A tall structure that serves as a guard post.");

        public static BuildingType Watergate { get; } = new BuildingType(Constants.BuildingTypeNames.Watergate, 1, 2, 0, "A gate in a City Wall that allows water (such as a river, Aqueduct, or Waterway) to enter the settlement. a Watergate has underwater defenses to block unwanted access. If you construct a Watergate when you construct a City Wall, the Watergate does not count toward the limit of the number of buildings you can construct per turn.");

        public static BuildingType Waterfront { get; } = new BuildingType(Constants.BuildingTypeNames.Waterfront, 12, 6, 4, "A port for waterborne arrival and departure, with facilities for shipping and shipbuilding.");

        public static BuildingType Waterway { get; } = new BuildingType(Constants.BuildingTypeNames.Waterway, 1, 3, 1, "A river or canal occupying part of the District Grid. At the GM’s option, a natural Waterway may already exist on the grid, requiring no action or BP to build. If you construct a City Wall that touches or crosses the Waterway, you must also build Watergates on the same tur");

        public static BuildingType Windmill { get; } = new BuildingType(Constants.BuildingTypeNames.Windmill, 1, 6, 1, "A wind-driven mill for grinding grain or pumping water.");

        #endregion

        public static Dictionary<string, BuildingType> AllBuildingTypes {
            get {
                if (_buildingTypes != null)
                {
                    return _buildingTypes;
                }

                _buildingTypes = new Dictionary<string, BuildingType> {
                    { Constants.BuildingTypeNames.Academy, Academy },
                    { Constants.BuildingTypeNames.Alchemist, Alchemist },
                    { Constants.BuildingTypeNames.Arena, Arena },
                    { Constants.BuildingTypeNames.Bank, Bank },
                    { Constants.BuildingTypeNames.BardicCollege, BardicCollege },
                    { Constants.BuildingTypeNames.Barracks, Barracks },
                    { Constants.BuildingTypeNames.Baths, Baths },
                    { Constants.BuildingTypeNames.BlackMarket, BlackMarket },
                    { Constants.BuildingTypeNames.Brewery, Brewery },
                    { Constants.BuildingTypeNames.Bridge, Bridge },
                    { Constants.BuildingTypeNames.Brothel, Brothel },
                    { Constants.BuildingTypeNames.Bureau, Bureau },
                    { Constants.BuildingTypeNames.CastersTower, CastersTower },
                    { Constants.BuildingTypeNames.Castle, Castle },
                    { Constants.BuildingTypeNames.Cathedral, Cathedral },
                    { Constants.BuildingTypeNames.Cistern, Cistern },
                    { Constants.BuildingTypeNames.CityWall, CityWall },
                    { Constants.BuildingTypeNames.Courthouse, Courthouse },
                    { Constants.BuildingTypeNames.Dump, Dump },
                    { Constants.BuildingTypeNames.EverflowingSpring, EverflowingSpring },
                    { Constants.BuildingTypeNames.ExoticArtisan, ExoticArtisan },
                    { Constants.BuildingTypeNames.ForeignQuarter, ForeignQuarter },
                    { Constants.BuildingTypeNames.Foundry, Foundry },
                    { Constants.BuildingTypeNames.Garrison, Garrison },
                    { Constants.BuildingTypeNames.Granary, Granary },
                    { Constants.BuildingTypeNames.Graveyard, Graveyard },
                    { Constants.BuildingTypeNames.Guildhall, Guildhall },
                    { Constants.BuildingTypeNames.Herbalist, Herbalist },
                    { Constants.BuildingTypeNames.Hospital, Hospital },
                    { Constants.BuildingTypeNames.House, House },
                    { Constants.BuildingTypeNames.Inn, Inn },
                    { Constants.BuildingTypeNames.Jail, Jail },
                    { Constants.BuildingTypeNames.Library, Library },
                    { Constants.BuildingTypeNames.Lighthouse, Lighthouse },
                    { Constants.BuildingTypeNames.Lumberyard, Lumberyard },
                    { Constants.BuildingTypeNames.LuxuryStore, LuxuryStore },
                    { Constants.BuildingTypeNames.MagicShop, MagicShop },
                    { Constants.BuildingTypeNames.MagicalAcademy, MagicalAcademy },
                    { Constants.BuildingTypeNames.MagicStreetlamps, MagicStreetlamps },
                    { Constants.BuildingTypeNames.Mansion, Mansion },
                    { Constants.BuildingTypeNames.Market, Market },
                    { Constants.BuildingTypeNames.Menagerie, Menagerie },
                    { Constants.BuildingTypeNames.MilitaryAcademy, MilitaryAcademy },
                    { Constants.BuildingTypeNames.Mill, Mill },
                    { Constants.BuildingTypeNames.Mint, Mint },
                    { Constants.BuildingTypeNames.Moat, Moat },
                    { Constants.BuildingTypeNames.Monastery, Monastery },
                    { Constants.BuildingTypeNames.Monument, Monument },
                    { Constants.BuildingTypeNames.Museum, Museum },
                    { Constants.BuildingTypeNames.NobleVilla, NobleVilla },
                    { Constants.BuildingTypeNames.Observatory, Observatory },
                    { Constants.BuildingTypeNames.Palace, Palace },
                    { Constants.BuildingTypeNames.Park, Park },
                    { Constants.BuildingTypeNames.PavedStreets, PavedStreets },
                    { Constants.BuildingTypeNames.Pier, Pier },
                    { Constants.BuildingTypeNames.SewerSystem, SewerSystem },
                    { Constants.BuildingTypeNames.Shop, Shop },
                    { Constants.BuildingTypeNames.Shrine, Shrine },
                    { Constants.BuildingTypeNames.Smithy, Smithy },
                    { Constants.BuildingTypeNames.Stable, Stable },
                    { Constants.BuildingTypeNames.Stockyard, Stockyard },
                    { Constants.BuildingTypeNames.Tavern, Tavern },
                    { Constants.BuildingTypeNames.Temple, Temple },
                    { Constants.BuildingTypeNames.Tenement, Tenement },
                    { Constants.BuildingTypeNames.Theater, Theater },
                    { Constants.BuildingTypeNames.TownHall, TownHall },
                    { Constants.BuildingTypeNames.TradeShop, TradeShop },
                    { Constants.BuildingTypeNames.Tunnels, Tunnels },
                    { Constants.BuildingTypeNames.University, University },
                    { Constants.BuildingTypeNames.Warehouse, Warehouse },
                    { Constants.BuildingTypeNames.Watchtower, Watchtower },
                    { Constants.BuildingTypeNames.Watergate, Watergate },
                    { Constants.BuildingTypeNames.Waterfront, Waterfront },
                    { Constants.BuildingTypeNames.Waterway, Waterway },
                    { Constants.BuildingTypeNames.Windmill, Windmill }
                };

                return _buildingTypes;
            }
        }
    }
    //I tried to make a dictionary but I fucked it up. We need a dictionary to track the modifiers from each building.
    //We can either do all the modifiers apply to the cities, and we add the relevant city modifiers together to get kingdom modifiers, or we can add just the city modifiers to the city, and the kingdom modifiers to the kingdom.
 
}
