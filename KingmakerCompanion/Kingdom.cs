using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Runtime.Serialization;

namespace KingmakerCompanion
{
    [Serializable]
    public class Kingdom
    {
        // yup im lost

        public string KingdomName { get; set; }
        public string Alignment { get; set; }
        public Hex StarterHex { get; set; }
        public List<Hex> KingdomHexes { get; set; } = new List<Hex>();
    }

    //TODO: move to own file
    public static class KingdomConstants
    {
        public static class Alignments
        {
            public const string ChaoticEvil = "Chaotic Evil";
            public const string ChaoticGood = "Chaotic Good";
            public const string ChaoticNeutral = "Chaotic Neutral";
            public const string LawfulEvil = "Lawful Evil";
            public const string LawfulGood = "Lawful Good";
            public const string LawfulNeutral = "Lawful Neutral";
            public const string NeutralEvil = "Neutral Evil";
            public const string NeutralGood = "Neutral Good";
            public const string TrueNeutral = "True Neutral";
        }

        public static class HexType
        {
            // how do these modifiers affect the hex?
            //They don't affect the hex, but only certain "improvements" can be built on rivers, and rivers provide bonuses if you have enough of them
            //Same with coasts - Really, the issue is that rivers and coasts aren't hexes, they are just conditions for other hexes
            //Needs like a checkbox that the user can select when adding a hex
            //Totally forgot, there is also the cavern modifier. But that kind of counts as its own hex, underneath another

            //we need a way to assign values linked to the hex type as well
            //since they cost different amounts of BP to claim
            //River and coast are modifiers to other hexes
            public const string River = "River";
            public const string Coast = "Coast";
            public const string Cavern = "Cavern";
            public const int CavernCost = 8;
            public const string Desert = "Desert";
            public const int DesertCost = 4;
            public const string Forest = "Forest";
            public const int ForestCost = 4;
            public const string Hills = "Hills";
            public const int HillsCost = 2;
            public const string Jungle = "Jungle";
            public const int JungleCost = 12;
            public const string Marsh = "Marsh";
            public const int MarshCost = 8;
            public const string Mountains = "Mountains";
            public const int MountainsCost = 12;
            public const string Plains = "Plains";
            public const int PlainsCost = 1;
            

        }

        public static class ImprovementType
        {
            public const string Aqueduct = "Aqueduct";
            public const string Canal = "Canal";
            public const string Farm = "Farm";
            public const int FarmCostPlains = 2;
            public const int FarmCostHills = 4;
            public const int FarmCostDesert = 8;
            public const string Fishery = "Fishery";
            public const int FisheryCost = 4;
            public const string Fort = "Fort";
            public const int FortCost = 24;
            public const string Highway = "Highway";
            public const string Mine = "Mine";
            public const int MineCost = 6;
            public const string Quarry = "Quarry";
            public const int QuarryCost = 6;
            public const string Road = "Road";
            public const string Sawmill = "Sawmill";
            public const int SawmillCost = 3;
            public const string Watchtower = "Watchtower";
            public const int WatchtowerCost = 12;
        }
    }
}
