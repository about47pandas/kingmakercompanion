﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingmakerCompanion
{
    [Serializable]
    public class Hex : INotifyPropertyChanged
    {
        private string _x;
        private int _y;
        private string _hexType;

        public string X {
            get => _x;
            set {
                _x = value;
                OnPropertyChanged(nameof(X));
            }
        }

        public int Y {
            get => _y;
            set {
                _y = value;
                OnPropertyChanged(nameof(Y));
            }
        }

        public string HexType {
            get => _hexType;
            set {
                _hexType = value;
                OnPropertyChanged(nameof(HexType));
            }
        }

        public event PropertyChangedEventHandler PropertyChanged;

        private void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }

        public override string ToString()
        {
            return X + Y.ToString();
        }
    }
}
