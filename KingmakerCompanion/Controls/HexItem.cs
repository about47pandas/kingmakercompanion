﻿using System.Windows;
using System.Windows.Controls;

namespace KingmakerCompanion.Controls
{
    public class HexItem : ListBoxItem
    {
        static HexItem()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(HexItem), new FrameworkPropertyMetadata(typeof(HexItem)));
        }
    }
}
