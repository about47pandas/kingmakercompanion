﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KingmakerCompanion
{
    public static class Constants
    {
        public static class BuildingTypeNames {
            public const string Academy = "Academy";
            public const string Alchemist = "Alchemist";
            public const string Arena = "Arena";
            public const string Bank = "Bank";
            public const string BardicCollege = "Bardic College";
            public const string Barracks = "Barracks";
            public const string Baths = "Baths";
            public const string BlackMarket = "BlackMarket";
            public const string Brewery = "Brewery";
            public const string Bridge = "Bridge";
            public const string Brothel = "Brothel";
            public const string Bureau = "Bureau";
            public const string CastersTower = "Caster's Tower";
            public const string Castle = "Castle";
            public const string Cathedral = "Cathedral";
            public const string Cistern = "Cistern";
            public const string CityWall = "City Wall";
            public const string Courthouse = "Courthouse";
            public const string Dump = "Dump";
            public const string EverflowingSpring = "Everflowing Spring";
            public const string ExoticArtisan = "Exotic Artisan";
            public const string ForeignQuarter = "Foreign Quarter";
            public const string Foundry = "Foundry";
            public const string Garrison = "Garrison";
            public const string Granary = "Granary";
            public const string Graveyard = "Graveyard";
            public const string Guildhall = "Guildhall";
            public const string Herbalist = "Herbalist";
            public const string Hospital = "Hospital";
            public const string House = "House";
            public const string Inn = "Inn";
            public const string Jail = "Jail";
            public const string Library = "Library";
            public const string Lighthouse = "Lighthouse";
            public const string Lumberyard = "Lumberyard";
            public const string LuxuryStore = "Luxury Store";
            public const string MagicShop = "Magic Shop";
            public const string MagicalAcademy = "Magical Academy";
            public const string MagicStreetlamps = "Magical Streetlamps";
            public const string Mansion = "Mansion";
            public const string Market = "Market";
            public const string Menagerie = "Menagerie";
            public const string MilitaryAcademy = "Military Academy";
            public const string Mill = "Mill";
            public const string Mint = "Mint";
            public const string Moat = "Moat";
            public const string Monastery = "Monastery";
            public const string Monument = "Monument";
            public const string Museum = "Museum";
            public const string NobleVilla = "Noble Villa";
            public const string Observatory = "Observatory";
            public const string Palace = "Palace";
            public const string Park = "Park";
            public const string PavedStreets = "Paved Streets";
            public const string Pier = "Pier";
            public const string SewerSystem = "Sewer System";
            public const string Shop = "Shop";
            public const string Shrine = "Shrine";
            public const string Smithy = "Smithy";
            public const string Stable = "Stable";
            public const string Stockyard = "Stockyard";
            public const string Tavern = "Tavern";
            public const string Temple = "Temple";
            public const string Tenement = "Tenement";
            public const string Theater = "Theater";
            public const string TownHall = "Town Hall";
            public const string TradeShop = "Trade Shop";
            public const string Tunnels = "Tunnels";
            public const string University = "University";
            public const string Warehouse = "Warehouse";
            public const string Watchtower = "Watchtower";
            public const string Watergate = "Watergate";
            public const string Waterfront = "Waterfront";
            public const string Waterway = "Waterway";
            public const string Windmill = "Windmill";
        }

        public static class FarmTileTypes {
            public const string Plains = "Plains";
            public const string Hills = "Hills";
            public const string Desert = "Desert";
        }
    }
}
