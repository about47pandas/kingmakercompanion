﻿using KingmakerCompanion.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace KingmakerCompanion
{
	/// <summary>
	/// Interaction logic for MainWindow.xaml
	/// </summary>
	public partial class MainWindow : Window
	{
        public List<Kingdom> Kingdoms { get; private set; } = new List<Kingdom>();

        public MainWindow()
		{
			InitializeComponent();
            // what are the hex types?

            // ok so on the newkingdom window, I was thinking the name, alignment, and location
            //location will be a 2 character letter/number code to match the map
            //Oh, I suppose it will also have to include the type of hex, since you need a hex to found a kingdom
            //plains, hills, mountains, desert, forest, jungle, swamp, ocean
            //Hexes can also include a coast, river, or both

            //Board.ItemsSource =
            //    Enumerable.Range(0, Board.RowCount)
            //        .SelectMany(r => Enumerable.Range(0, Board.ColumnCount)
            //            .Select(c => new IntPoint(c, r)))
            //        .ToList();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            //var newKingdomWindow = new NewKingdom();
            //newKingdomWindow.Open();
        }

        private void NumberValidationTextBox(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
