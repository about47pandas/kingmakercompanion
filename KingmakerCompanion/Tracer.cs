using System;
using System.IO;

namespace DonJonGM.Debug
{
    public static class Tracer
    {
        private static bool _logExists;

        private static string _sName;

        public static string Name => Path.Combine(Environment.CurrentDirectory, _sName + "Logs.txt");

        public static void Initialize(string logName)
        {
            _sName = logName;
#if DEBUG
            AppDomain.CurrentDomain.UnhandledException += UnexpectedFailure;
            if (!_logExists) {
                CreateLogIfDoesntExist();
            }
#endif
        }

        public static void TraceContentWrite(string description, object thing)
        {
            TraceMain(description + " ({0})", new[] { thing }, "TraceContentWrite: ", ConsoleColor.DarkBlue);
        }

        public static void TraceGeneric(string format, params object[] args)
        {
            TraceMain(format, args, "TraceGeneric: ", ConsoleColor.White);
        }

        public static void TraceException(Exception e, string format, params object[] args)
        {
            TraceMain(format, args, $"TraceException({e.GetType()}): {e}, ", ConsoleColor.Red);
        }

        public static void TraceError(string format, params object[] args)
        {
            TraceMain(format, args, "TraceError: ", ConsoleColor.Yellow);
        }

        public static void TraceCommand(string format, params object[] args)
        {
            TraceMain(format, args, "TraceCommand: ", ConsoleColor.Green);
        }

        public static void UnexpectedFailure(object sender, UnhandledExceptionEventArgs e)
        {
            TraceException((Exception)(e.ExceptionObject), "Unexpected failure while running");
        }

        private static void TraceMain(string format, object[] args, string traceString, ConsoleColor color)
        {
#if DEBUG
            if (_sName == null) {
                throw new InvalidOperationException("Tracer must be initialized with a name via Tracer.Initialize(string logName) method.");
            }

            if (!_logExists) {
                CreateLogIfDoesntExist();
            }

            Trace(format, args, traceString, color);
#endif
        }

        private static void CreateLogIfDoesntExist()
        {
            if (!File.Exists(Name))
            {
                if (!Directory.Exists(Path.GetDirectoryName(Name)))
                {
                    Directory.CreateDirectory(Path.GetDirectoryName(Name));
                }
                using (FileStream fs = new FileStream(Name, FileMode.OpenOrCreate, FileAccess.ReadWrite))
                {
                    using (BinaryWriter w = new BinaryWriter(fs))
                    {
                        w.Write(string.Format("Log File Created On ({0})\n", System.DateTime.Now));
                    }
                }
            }
            _logExists = true;
        }

        private static void Trace(string format, object[] args, string traceString, ConsoleColor color)
        {
            using (StreamWriter sw = File.AppendText(Name))
            {
                string writeToString = string.Format(traceString + format, args);
                sw.WriteLine("(" + DateTime.Now.ToShortDateString() + ", " + DateTime.Now.ToLongTimeString() + ") : " + writeToString);
                Console.ForegroundColor = color;
                Console.WriteLine(writeToString);
                Console.ForegroundColor = ConsoleColor.White;
            }
        }
    }
}
