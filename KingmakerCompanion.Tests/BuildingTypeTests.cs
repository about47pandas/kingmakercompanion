﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace KingmakerCompanion.Tests
{
    [TestClass]
    public class BuildingTypeTests
    {
        [TestMethod]
        public void BuildingTypes_DictionaryIsUnique()
        {
            int totalLoops = 0;
            bool isUnique = true;
            foreach (KeyValuePair<string, BuildingType> buildingPair in BuildingType.AllBuildingTypes)
            {
                foreach (KeyValuePair<string, BuildingType> otherPair in BuildingType.AllBuildingTypes.Where(bp => bp.Key != buildingPair.Key))
                {
                    totalLoops++;

                    isUnique = buildingPair.Value != otherPair.Value;

                    if (!isUnique)
                    {
                        break;
                    }
                }

                if (!isUnique)
                {
                    break;
                }
            }
            Console.Write($"total loops: {totalLoops}");
            Assert.IsTrue(isUnique);
        }
    }
}
